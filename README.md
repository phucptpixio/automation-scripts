# Automation script

This script allow you to automatically clear the cloudflare cache, put it in your directory

## Cloudflare Purging Cache Script

![purge-cache](./purge-cache.png)

To run this script you will need to provide `Zone ID` and `TOKEN` from cloudflare

* [How to obtain Zone ID](https://community.cloudflare.com/t/where-to-find-zone-id/132913)
* [How to get TOKEN](https://support.cloudflare.com/hc/en-us/articles/200167836-Managing-API-Tokens-and-Keys)

## Usage

When you got the `Zone ID` and `TOKEN`. run the script with the following format


```
/bin/bash clear-cache-cloudflare.sh $YOUR_ZONE_ID $YOUR_TOKEN
```


`SUCCESS`

![Success](./run-success.png)


`FAILED`

![Failed](./run-failed.png)
